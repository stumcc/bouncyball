package org.sonatype.mavenbook;

import java.util.Scanner;
import static java.lang.System.out;
import java.io.IOException;

public class App {
	
	public static void main(String args[]) throws IOException{
		//Instantiate some objects
		String InputString = "g";
		int width_max = 3;
		int height_max = 3;
		
		boolean bError = true;
		Scanner keyboard = new Scanner(System.in);
		
		String myGraphic = "/bouncy_ball.txt";
		FilePrint.ConsoleDump(myGraphic);
		
		out.println("***Type in the width!***");
		
		while (bError){
			InputString = keyboard.nextLine();
			if(Handy.isInteger(InputString)){
				width_max = Integer.valueOf(InputString);
				if((width_max<3)||(width_max>20)){
					out.println("Between 3 and 20 please!");
				}
				else{
				bError  = false;
				}
			}
			else{
				out.println("That's not a number!");
			}
		}
					
		bError = true;
		keyboard = new Scanner(System.in);
		System.out.println("***Thankyou! Now type in the height! And we'll start!***");

		while (bError){
			InputString = keyboard.nextLine();
			if(Handy.isInteger(InputString)){
				height_max = Integer.valueOf(InputString);
				if((height_max<3)||(height_max>20)){
					out.println("Between 3 and 20 please!");
				}
				else{
				bError  = false;
				}
			}
			else{
				out.println("That's not a number!");
			}
		}		
		
		
		
		int moves = 0;
		coordinate coord = new coordinate(1,height_max);
		int current_x;
		int current_y;
		int width_min = 1;
		int height_min = 1;
		int x_increment = 1;
		int y_increment = -1;
		int ricochets = 0;
		float time;
		coords_printer myPrinter = new coords_printer(width_max,height_max);
		coord.print_coords();
		while (true){
			moves++;
			coord.increment_coordinates(x_increment,y_increment);
			coord.print_coords();
			current_x = coord.get_x();
			current_y = coord.get_y();
			if((current_x == width_min)&&(current_y == height_min)) {break;}
			if((current_x == width_min)&&(current_y == height_max)) {break;}
			if((current_x == width_max)&&(current_y == height_min)) {break;}
			if((current_x == width_max)&&(current_y == height_max)) {break;}			
			if(current_x == width_min ) {x_increment = 1; ricochets++; myPrinter.print_grid(current_x,current_y);}
			if(current_x == width_max) {x_increment = -1; ricochets++; myPrinter.print_grid(current_x,current_y);}
			if(current_y == height_min) {y_increment = 1; ricochets++; myPrinter.print_grid(current_x,current_y);}
			if(current_y == height_max) {y_increment = -1; ricochets++; myPrinter.print_grid(current_x,current_y);}
		}
		System.out.println("***BOUNCY BALL COMPLETE!***");
		System.out.print("There were ");
		System.out.print(ricochets);
		System.out.println(" ricochets.");
		System.out.print("Journey completed in ");
		System.out.print(moves);
		System.out.print(" hops");
		
		
		
		
		
		
		
	}
	
	
	
	
}