package org.sonatype.mavenbook;

import static java.lang.System.out;

public class coords_printer {
	
	private int width;
	private int height;
	
	//constructor to obtain the width and height of the grid
	
	public coords_printer(int width_in, int height_in){
		
	this.width = width_in;
	this.height = height_in;
	}
	
	//method below prints out a picture to show whereabouts the ricochet happened
	
	public void print_grid(int x, int y) {
		out.println("***RICOCHET***");
		for (int j = height; j>=1; j--)	{
			for (int i = 1; i<=width; i++)	{
				if((i == x)&&(j == y)) 
					{out.print("x ");}
				else 
					{out.print("o ");}	
			}
			out.println();
		}
		
		
	}
	
}